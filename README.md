## Project Information

### Create Ansible Roles

Individual files are located separately. 

```
ansible-project/
├── roles/
│   ├── mysql/
│   │   ├── tasks/
│   │   │   └── main.yml
│   │   ├── handlers/
│   │   │   └── main.yml
│   │   ├── templates/
│   │   ├── vars/
│   │   │   └── main.yml
│   │   └── defaults/
│   │       └── main.yml
│   └── webapp/
│       ├── tasks/
│       │   └── main.yml
│       ├── handlers/
│       │   └── main.yml
│       ├── templates/
│       ├── vars/
│       │   └── main.yml
│       └── defaults/
│           └── main.yml
├── hosts
└── site.yml
```

### Ansible Vault
According to role creation specification, variables have to be stored under ./vars/main.yml. We have, therefore, saved the db credentials there and encrypted them using this command:

```
ansible-vault encrypt roles/webapp/vars/main.yml
```
Now, whenever we use --ask-vault-pass, it will decrypt vault file and include the environment variables.
```
ansible-playbook site.yml -i hosts --ask-vault-pass
```

### Docker Integration
The webapp role clones the project to remote server where it builds the image and runs the container. 
The images are built and re-run only if there are changes to the remote repo after the first built. 

### Application Deployment
The database is deployed on the host vm itself, where the app is running on a dockerized environment. 
To check if the app is running:
```
root@devops:~/lab3# curl -X POST lab.mamedius.site:3000/api/v1/tasks -d '{"task": "taskk"}' -H "Content-Type: application/json"
OK
root@devops:~/lab3# curl http://lab.mamedius.site:3000/api/v1/tasks
[{"id":1,"task":"hey"},{"id":2,"task":"hey"},{"id":3,"task":"hey"},{"id":4,"task":"hey"},{"id":5,"task":"taskk"}]
```


### Example Execution
```
root@ansible:~/ansible-project# ansible-playbook site.yml -i hosts --ask-vault-pass
Vault password:
[WARNING]: Invalid characters were found in group names but not replaced, use -vvvv to see details

PLAY [Update APT package cache] *****************************************************************************************************

TASK [Gathering Facts] **************************************************************************************************************
ok: [prod-server]

TASK [Update APT cache] *************************************************************************************************************
ok: [prod-server]

PLAY [Configure Database Servers] ***************************************************************************************************

TASK [Gathering Facts] **************************************************************************************************************
ok: [prod-server]

TASK [mysql : ansible.builtin.include_tasks] ****************************************************************************************
included: /root/ansible-project/roles/mysql/tasks/variables.yml for prod-server

TASK [mysql : Include OS-specific variables.] ***************************************************************************************
ok: [prod-server] => (item=/root/ansible-project/roles/mysql/vars/Debian-11.yml)

TASK [mysql : Define mysql_packages.] ***********************************************************************************************
ok: [prod-server]

TASK [mysql : Define mysql_daemon.] *************************************************************************************************
ok: [prod-server]

TASK [mysql : Define mysql_slow_query_log_file.] ************************************************************************************
ok: [prod-server]

TASK [mysql : Define mysql_log_error.] **********************************************************************************************
ok: [prod-server]

TASK [mysql : Define mysql_syslog_tag.] *********************************************************************************************
ok: [prod-server]

TASK [mysql : Define mysql_pid_file.] ***********************************************************************************************
ok: [prod-server]

TASK [mysql : Define mysql_config_file.] ********************************************************************************************
ok: [prod-server]

TASK [mysql : Define mysql_config_include_dir.] *************************************************************************************
ok: [prod-server]

TASK [mysql : Define mysql_socket.] *************************************************************************************************
ok: [prod-server]

TASK [mysql : Define mysql_supports_innodb_large_prefix.] ***************************************************************************
ok: [prod-server]

TASK [mysql : ansible.builtin.include_tasks] ****************************************************************************************
skipping: [prod-server]

TASK [mysql : ansible.builtin.include_tasks] ****************************************************************************************
included: /root/ansible-project/roles/mysql/tasks/setup-Debian.yml for prod-server

TASK [mysql : Check if MySQL is already installed.] *********************************************************************************
ok: [prod-server]

TASK [mysql : Update apt cache if MySQL is not yet installed.] **********************************************************************
skipping: [prod-server]

TASK [mysql : Ensure MySQL Python libraries are installed.] *************************************************************************
ok: [prod-server]

TASK [mysql : Ensure MySQL packages are installed.] *********************************************************************************
ok: [prod-server]

TASK [mysql : Ensure MySQL is stopped after initial install.] ***********************************************************************
skipping: [prod-server]

TASK [mysql : Delete innodb log files created by apt package after initial install.] ************************************************
skipping: [prod-server] => (item=ib_logfile0)
skipping: [prod-server] => (item=ib_logfile1)

TASK [mysql : ansible.builtin.include_tasks] ****************************************************************************************
skipping: [prod-server]

TASK [mysql : Check if MySQL packages were installed.] ******************************************************************************
ok: [prod-server]

TASK [mysql : ansible.builtin.include_tasks] ****************************************************************************************
included: /root/ansible-project/roles/mysql/tasks/configure.yml for prod-server

TASK [mysql : Get MySQL version.] ***************************************************************************************************
ok: [prod-server]

TASK [mysql : Copy my.cnf global MySQL configuration.] ******************************************************************************
ok: [prod-server]

TASK [mysql : Verify mysql include directory exists.] *******************************************************************************
skipping: [prod-server]

TASK [mysql : Copy my.cnf override files into include directory.] *******************************************************************

TASK [mysql : Create slow query log file (if configured).] **************************************************************************
skipping: [prod-server]

TASK [mysql : Create datadir if it does not exist] **********************************************************************************
ok: [prod-server]

TASK [mysql : Set ownership on slow query log file (if configured).] ****************************************************************
skipping: [prod-server]

TASK [mysql : Create error log file (if configured).] *******************************************************************************
skipping: [prod-server]

TASK [mysql : Set ownership on error log file (if configured).] *********************************************************************
skipping: [prod-server]

TASK [mysql : Ensure MySQL is started and enabled on boot.] *************************************************************************
ok: [prod-server]

TASK [mysql : ansible.builtin.include_tasks] ****************************************************************************************
included: /root/ansible-project/roles/mysql/tasks/secure-installation.yml for prod-server

TASK [mysql : Ensure default user is present.] **************************************************************************************
ok: [prod-server]

TASK [mysql : Copy user-my.cnf file with password credentials.] *********************************************************************
skipping: [prod-server]

TASK [mysql : Disallow root login remotely] *****************************************************************************************
ok: [prod-server] => (item=DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1'))

TASK [mysql : Get list of hosts for the root user.] *********************************************************************************
skipping: [prod-server]

TASK [mysql : Update MySQL root password for localhost root account (5.7.x).] *******************************************************

TASK [mysql : Update MySQL root password for localhost root account (< 5.7.x).] *****************************************************

TASK [mysql : Copy .my.cnf file with root password credentials.] ********************************************************************
skipping: [prod-server]

TASK [mysql : Get list of hosts for the anonymous user.] ****************************************************************************
ok: [prod-server]

TASK [mysql : Remove anonymous MySQL users.] ****************************************************************************************

TASK [mysql : Remove MySQL test database.] ******************************************************************************************
ok: [prod-server]

TASK [mysql : ansible.builtin.include_tasks] ****************************************************************************************
included: /root/ansible-project/roles/mysql/tasks/databases.yml for prod-server

TASK [mysql : Ensure MySQL databases are present.] **********************************************************************************

TASK [mysql : ansible.builtin.include_tasks] ****************************************************************************************
included: /root/ansible-project/roles/mysql/tasks/users.yml for prod-server

TASK [mysql : Ensure MySQL users are present.] **************************************************************************************
skipping: [prod-server]

TASK [mysql : ansible.builtin.include_tasks] ****************************************************************************************
included: /root/ansible-project/roles/mysql/tasks/replication.yml for prod-server

TASK [mysql : Ensure replication user exists on master.] ****************************************************************************
skipping: [prod-server]

TASK [mysql : Check slave replication status.] **************************************************************************************
skipping: [prod-server]

TASK [mysql : Check master replication status.] *************************************************************************************
skipping: [prod-server]

TASK [mysql : Configure replication on the slave.] **********************************************************************************
skipping: [prod-server]

TASK [mysql : Start replication.] ***************************************************************************************************
skipping: [prod-server]

PLAY [Start web application] ********************************************************************************************************

TASK [Gathering Facts] **************************************************************************************************************
ok: [prod-server]

TASK [webapp : Clone the web application repository] ********************************************************************************
ok: [prod-server]

TASK [webapp : Build Docker Image for Webapp] ***************************************************************************************
skipping: [prod-server]

TASK [webapp : Run Webapp Container] ************************************************************************************************
[DEPRECATION WARNING]: The container_default_behavior option will change its default value from "compatibility" to "no_defaults" in
community.general 3.0.0. To remove this warning, please specify an explicit value for it now. This feature will be removed from
community.general in version 3.0.0. Deprecation warnings can be disabled by setting deprecation_warnings=False in ansible.cfg.
ok: [prod-server]

PLAY RECAP **************************************************************************************************************************
prod-server                : ok=36   changed=0    unreachable=0    failed=0    skipped=25   rescued=0    ignored=0
```
